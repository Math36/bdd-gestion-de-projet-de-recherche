Note de clarification : Gestion de projet de recherche (projet n°29)
=====================

Le but de ce système à terme, est de faciliter la gestion des projets pour le compte d'un laboratoire de recherche. Pour ce faire, nous regrouperons dans la base de données des informations sur les organismes de projets et les financeurs, mais aussi sur 
les membres et les équipes du laboratoire ainsi que sur les projets dont le laboratoire a la charge.

Les principales fonctionnalités qui seront permises par l'application seront :
- Pour les projets et les propositions de projets du laboratoire  :
    - Gérer les informations liées aux projets et aux propositions de projets
    - Suivre l'avancement des projets en cours
    - Suivre l'évolution du budget des projets en cours
    - Suivre les réponses aux offres de projet faites par le laboratoire
- Pour les équipes du laboratoire (interne/externe) :
    - Gérer les informations des membres
    - Gérer leurs affectations aux différents projets
    - Gérer leurs dépenses dans le cadre des projets
- Pour appels à projet :
    - Répertorier les appels à projet actuels
    - Gérer leurs informations
- Pour les organismes de projet :
    - Gérer les informations des organismes de projet
    - Répertorier les offres faites par les différents organismes
- Pour les financeurs :
    - Gérer les informations des financeurs


La liste des objets / associations :
------------

- **Organisme de projet** :
    - creation : Date
    - duree : integer NULL
    - nom : string {key}
    - existe : bool

*existe* permet d'identifier les **Organisme de projet** qui ont disparu de ceux qui existent. *duree* est exprimée en années.

- **Entité juridique** :
    - nom : string {key}

*nom* est un attribut artificiel rajouté pour servir de clé à la relation.

- **Financeurs** :
    - debutActivite : Date
    - finActivite : Date

*debutActivite* doit être inférieur à *finActivite*.
La relation **Financeur** hérite de la relation **Entité juridique**. Cet héritage sera traduit par un héritage par référence de **Financeur** vers **EntiteJuridique** (voir justification dans le ***MLD***).
1 **Organisme de projet** à plusieurs **Financeurs** et 1 **Financeur** a créée au moins 1 **Organisme de projet**, sinon ce n'est pas un **Financeur**.

- **Employé contact** :
    - titre : string {key}
    - mail : string
    - telephone : integer(10)

1 **Employé contact** peut correspondre à 1 **Financeur** et 1 **Financeur** pourra avoir plusieurs **Employé contact**.

- **Appel à projet** :
    - theme : string
    - description : string
    - dateLancement : Date
    - periode : integer

Le couple (theme,description) est la clé d'**Appel à projet**. *periode* est exprimée en jours.
1 **Appel à projet** est lancé par 1 **Organisme de projet** et 1 **Organisme de projet** peut avoir lancé 0 ou plusieurs **Appels à projet**.

- **Proposition** :
    - titre : string {key}
    - date : Date
    - dateReponse : Date
    - accepte : boolean

*titre* est un attribut artificiel rajouté pour servir de clé à la relation. *accepte* permettra de savoir si une proposition a reçue une proposition négative ou positive.
*dateReponse* doit être inférieure à *date*. La *date* d'une proposition doit être inférieure à la date de lancement + la période de l'**Appel à projet** correspondant.

- **Budget** :
    - montant : integer 
    - id : integer {key}

On rajoute cet id afin de différencier les budgets entre eux et conserver une cohérence lorsqu'on ajoutera les lignes budgétaires.

- **Lignes budgétaires** :
    - montant : integer
    - objet : string
    - type : {fonctionnement,matériel}

Le triplet (montant,objet,type) constitue la clé de lignes budgétaires. Il y a une association de composition entre **Budget** et **Ligne budgétaires**.
1 **Proposition** a 1 **Budget** mais 1 **Budget** peut avoir été attribué plusieurs fois à plusieurs **Propositions**.
1 **Proposition** correspond à 1 **Appel à projet** mais 1 **Appel à projet** pourra recevoir plusieurs **Propositions**.

- **Label** :
    - titre : string {key}

1 **Proposition** peut avoir plusieurs **Labels** ou aucun et 1 **Label** peut correspondre à plusieurs ou aucune **Proposition**.
1 **Label** est délivré par au moins 1 **Entité juridique**.

- **Membre** :
    - nom : string
    - fonction : string
    - mail : string

La clé de membre est le couple (nom,fonction) car le mail peut être amené à changer.
La classe membre fera l'objet d'une relation d'héritage selon que les membres fassent parti ou non du laboratoire :

- **Interne** :
- **Externe** :

Cet héritage sera implémenté sous la forme d'un héritage par référence depuis les classes filles vers la classe mère (voir justification dans le ***MLD***).
Parmi les membres **Internes**, on distingue encore trois types de membres qui hériteront de la relation **Interne** :

- **Enseignant-chercheur** :
    - quotité : integer
    - etablissement : string

- **Ingénieur** :
    - spécialité : string

- **Doctorant** :
    - debutThese : Date
    - finThese : Date NULL
    - sujetThese : string

L'héritage entre **Interne** et ses 3 classes filles sera traduit par un héritage par la classe mère (voir justifcation dans le ***MLD***).
*debutThese* doit être inférieur à *finThese*.
1 **Proposition** est rédigée par au moins 1 **Interne** et 1 **Interne** peut avoir rédigé 0 ou plusieurs **Propositions**.

- **Projet** :
    - dateDebut : Date
    - dateFin : Date
    - nom : string {key}

*dateDebut* doit être inférieur à *dateFin*.
On rajoute l'attribut fictif nom afin de différencier les projets entre eux. *dateDebut* doit être inférieure à *dateFin*.
Le budget d'un **Projet** devra être le même que celui attribué à la **Proposition de projet** à son origine. Un **Projet** ne peut être issu que d'une **Proposition de projet** qui a été acceptée.

- **Dépense** :
    - date : Date
    - montant : integer
    - type : {fonctionnement,matériel}

Le triplet (date,montant,type) est la clé de **Dépense**. Chaque **Dépense** doit être réalisée entre la date de début et la date de fin du **Projet** correspondant.
1 **Projet** peut avoir nécessité plusieurs **Dépenses** et 1 **Dépense** correspond à 1 **Projet**.
Il faut faire attention que la somme des **Dépenses** n'excèdent pas le budget du projet.
Chaque **Dépense** est liée à 1 **Membre** dit "demandeur" et à 1 autre dit "validateur" parmi les membres du projet.

1 **Projet** est réalisé par plusieurs **Membres** et 1 **Membre** peut avoir réalisé plusieurs **Projets**.
1 **Projet** est lié à une **Proposition de projet** mais une **Proposition de projet** ne se transforme pas forcément en **Projet**.
1 **Projet** est lié à 1 **Budget**, le même que celui de sa **Proposition de projet** mais un budget peut être attribué à 0 ou plusieurs **Projets** différents.

Les dates seront données au format JJ-MM-AAAA.

Vues
-----

Les différentes recherches réalisables seront permises par l'intermédiaire de vues :
- vBudgetNonNul -> les projets avec un budget supérieur à 0
- vProjetEnCours -> les projets qui ne sont pas encore finis
- vAppelEnCours -> les appels qui n'ont pas encore donné lieu à un projet

Les différentes requêtes statistiques nécessaires feront l'objet de vues.
On en distinguera plusieurs :
- vPropSelonOrganisme -> totalisant les propositions répondues selon le type d'organisme de projet
- vMembrePlusActif -> pour chaque membre du laboratoire, combien de projet en cours.
- vMoisAvecPlusDepense -> le mois de l'année où il y a le plus de dépenses de réalisées


Utilisateurs
------------

Nous distinguerons plusieurs utilisateurs qui pourront avoir un accés à cette base :
- Le gestionnaire informatique/informaticien. Il devra disposer de tous les droits.
- La direction du laboratoire qui devra disposer de tous les droits d'écriture et de lecture sur toutes les tables.
- Les entités juridiques qui devront disposer d'un simplement des droits de lecture sur les tables du package Projet (voir UML) ainsi que des droits d'écriture sur la table OrganismeDeProjet et AppelProjet.
- Les membres qui pourront avoir accés en lecture à toutes les tables des package Projet et Finance ainsi que Personne (voir UML). Ils disposeront des droits d'écriture sur les tables Proposition,
et Depense.

JSON
------

Les attributs pour lesquels j'ai décidé d'utiliser le format JSON sont les suivants :
- LigneBudgetaire deviendra un attribut JSON de Budget puisque qu'un budget est composé de lignes budgétaires. Cela permettra de réduire le nombre de relation de la base de données.
- L'héritage entre Interne et ses 3 classes filles sera transformé en un héritage par la classe mère en mettant les attributs des classes filles et le type au format JSON. 