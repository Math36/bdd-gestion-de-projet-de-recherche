Projet numéro 29 : Gestion de projet de recherche
====================

﻿Contexte 
-------------------
Le but de ce projet est de développer une base de données pour le compte d'un laboratoire. Elle devra lui permettre de mieux gérer les projets auxquels il participe en visualisant notamment l'évolution du budget 
et les membres qui y prennent part mais aussi de répertorier les orgranismes de projet qui attendent des propositions et les informations liées aux demandeurs.

Objet 
--------------------
Le système devra permettre d'accéder facilement aux différents projets et offres de projets ainsi qu'aux informations qui y sont liées. Il devra également permettre de gérer au sein du laboratoire, les membres qui 
participent au projet et de réaliser des recherches ou des requêtes statistiques sur ses projets.

Acteurs 
--------------------
**Maîtrise d'ouvrage** : Chargé de TD de l'UV NF17 à l'UTC, Alessandro Victorino 

**Maîtrise d'oeuvre** : VAST Mathias.

Contraintes 
--------------------
La maîtrise d'ouvrage exige que la gestion du projet se fasse sous Git, en particulier sur le serveur Gitlab de l'UTC. <br>
Les ressources du projet seront alors accessibles sous la forme d'une URL vers ce serveur Gitlab.  
Les bases de données relationnelles doivent être réalisées avec PostgreSQL et JSON, le code doit être compatible avec la version installée sur les serveurs de l'UTC.<br>
Les formats utilisés sont ceux imposés par la maîtrise d'ouvrage (*markdown*/*plaintext*)

Livrables 
-------------------
- **README.md**: un fichier comportant le nom des auteurs ainsi que toutes les informations nécessaires à la compréhension de l'architecture du projet. 
- **NDC**: Note de clarification au format markdown.
- **MCD**: un modèle UML au format *plantuml*
- **MLD Relationnel**: un modèle logique au format *plain text*
    - **MLDv1** : le premier jet du modèle logique
    - **MLDv2** : le modèle logique normalisé
- **BDD**: la base de données détaillant la construction des tables, vues, données de test et questions attendues avec les requêtes SQL (vues)
- **Complément**: *PostgreSQL* et *JSON*
- Un fichier **exec.sql** qui contient le code éxécutable afin de créer tous les éléments de la base de données qui sont dans le dossier **BDD**
- Un dossier **Screen** qui contient les captures d'écran de la base de données une fois testée avec DB Disco.

Les livrables seront situés dans la branche master du répertoire.

