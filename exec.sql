                            /*---------------------*/
                            /* Création des tables */
                            /*---------------------*/

                            /*-------------------------------*/
                            /* Relations et association 1..1 */
                            /*-------------------------------*/

CREATE TYPE Type AS ENUM ('Fonctionnement','Materiel');

CREATE TABLE Budget(
 id INTEGER PRIMARY KEY,
 montant INTEGER NOT NULL,
 lignesBudgetaires JSON NOT NULL,
 CHECK (montant>0)
);

CREATE TABLE EntiteJuridique(
 nom VARCHAR PRIMARY KEY
);

CREATE TABLE Financeur(
 nomEntite VARCHAR PRIMARY KEY,
 debutActivite DATE NOT NULL,
 finActivite DATE NOT NULL,
 FOREIGN KEY (nomEntite) REFERENCES EntiteJuridique,
 CHECK (debutActivite<finActivite)
);

CREATE TABLE EmployeContact(
 titre VARCHAR,
 mail VARCHAR NOT NULL,
 telephone VARCHAR(10) NOT NULL,
 financeur VARCHAR NOT NULL,
 PRIMARY KEY(titre,financeur),
 FOREIGN KEY (financeur) REFERENCES Financeur
);

CREATE TABLE OrganismeDeProjet(
 nom VARCHAR PRIMARY KEY,
 creation DATE NOT NULL,
 duree INTEGER,
 existe BOOLEAN NOT NULL,
 CHECK(duree>0)
);

CREATE TABLE AppelProjet(
 theme VARCHAR,
 description VARCHAR,
 dateLancement DATE NOT NULL,
 periode INTEGER NOT NULL,
 organisme VARCHAR NOT NULL,
 PRIMARY KEY(theme,description),
 FOREIGN KEY (organisme) REFERENCES OrganismeDeProjet
);

CREATE TABLE Proposition(
 titre VARCHAR PRIMARY KEY,
 date DATE NOT NULL,
 dateReponse DATE,
 accepte BOOLEAN NOT NULL,
 budget INTEGER UNIQUE NOT NULL,
 themeAppel VARCHAR NOT NULL,
 descriptionAppel VARCHAR NOT NULL,
 FOREIGN KEY (themeAppel,descriptionAppel) REFERENCES AppelProjet(theme,description),
 CHECK (dateReponse>date OR dateReponse IS NULL)
);

CREATE TABLE Projet(
 nom VARCHAR PRIMARY KEY,
 dateDebut DATE NOT NULL,
 dateFin DATE NOT NULL,
 proposition VARCHAR NOT NULL,
 FOREIGN KEY (proposition) REFERENCES Proposition,
 CHECK (dateDebut<dateFin)
);

CREATE VIEW vProjet AS
SELECT P.nom, P.dateDebut, P.dateFin, P.proposition, Proposition.budget
FROM Projet P, Proposition
WHERE P.proposition=Proposition.titre
GROUP BY P.nom,Proposition.budget;

CREATE TABLE Label(
 titre VARCHAR PRIMARY KEY
);

CREATE TABLE Membre(
 nom VARCHAR NOT NULL,
 fonction VARCHAR NOT NULL,
 mail VARCHAR NOT NULL,
 PRIMARY KEY (nom,fonction)
);

CREATE TABLE Interne(
 nom VARCHAR NOT NULL,
 fonction VARCHAR NOT NULL,
 statut JSON NOT NULL,
 PRIMARY KEY (nom,fonction),
 FOREIGN KEY (nom,fonction) REFERENCES Membre(nom,fonction)
);

CREATE TABLE Externe(
 nom VARCHAR NOT NULL,
 fonction VARCHAR NOT NULL,
 PRIMARY KEY (nom,fonction),
 FOREIGN KEY (nom,fonction) REFERENCES Membre(nom,fonction)
);

CREATE TABLE Depense(
 date DATE NOT NULL,
 montant INTEGER NOT NULL,
 type Type NOT NULL,
 projet VARCHAR NOT NULL,
 nomDemandeur VARCHAR NOT NULL,
 fonctionDemandeur VARCHAR NOT NULL,
 nomValidateur VARCHAR NOT NULL,
 fonctionValidateur VARCHAR NOT NULL,
 PRIMARY KEY (date,montant,type),
 FOREIGN KEY (projet) REFERENCES Projet,
 FOREIGN KEY (nomDemandeur,fonctionDemandeur) REFERENCES Membre(nom,fonction),
 FOREIGN KEY (nomValidateur,fonctionValidateur) REFERENCES Membre(nom,fonction),
 CHECK(montant>0)
);

                            /*------------------*/
                            /* Association N..N */
                            /*------------------*/

CREATE TABLE MembreContribueAProjet(
 nomMembre VARCHAR NOT NULL,
 fonctionMembre VARCHAR NOT NULL,
 nomProjet VARCHAR NOT NULL,
 PRIMARY KEY(nomMembre,fonctionMembre,nomProjet),
 FOREIGN KEY(nomMembre,fonctionMembre) REFERENCES Membre,
 FOREIGN KEY(nomProjet) REFERENCES Projet
);

CREATE TABLE InternePropose(
 nomMembre VARCHAR NOT NULL,
 fonctionMembre VARCHAR NOT NULL,
 titreProposition VARCHAR NOT NULL,
 PRIMARY KEY(nomMembre,fonctionMembre,titreProposition),
 FOREIGN KEY(nomMembre,fonctionMembre) REFERENCES Interne,
 FOREIGN KEY(titreProposition) REFERENCES Proposition
);

CREATE TABLE LabelProposition(
 titreProposition VARCHAR NOT NULL,
 titreLabel VARCHAR NOT NULL,
 PRIMARY KEY (titreProposition,titreLabel),
 FOREIGN KEY(titreProposition) REFERENCES Proposition,
 FOREIGN KEY (titreLabel) REFERENCES Label
);

CREATE TABLE EntiteDelivreLabel(
 nomEntite VARCHAR NOT NULL,
 titreLabel VARCHAR NOT NULL,
 PRIMARY KEY (nomEntite,titreLabel),
 FOREIGN KEY(nomEntite) REFERENCES EntiteJuridique,
 FOREIGN KEY (titreLabel) REFERENCES Label
);

CREATE TABLE FinanceOrganisme(
 nomFinanceur VARCHAR NOT NULL,
 nomOrganisme VARCHAR NOT NULL,
 PRIMARY KEY(nomFinanceur,nomOrganisme),
 FOREIGN KEY(nomFinanceur) REFERENCES Financeur,
 FOREIGN KEY(nomOrganisme) REFERENCES OrganismeDeProjet
);

                            /*------------------*/
                            /* Vues héritages */
                            /*------------------*/
                            
CREATE VIEW vFinanceur AS
SELECT * FROM Financeur,EntiteJuridique
WHERE Financeur.nomEntite=EntiteJuridique.nom
GROUP BY Financeur.nomEntite,EntiteJuridique.nom;

CREATE VIEW vMembreInterne AS
SELECT M.nom, M.fonction FROM Membre M, Interne
WHERE M.nom=Interne.nom AND M.fonction=Interne.fonction;

CREATE VIEW vMembreExterne AS
SELECT M.nom, M.fonction FROM Membre M, Externe
WHERE M.nom=Externe.nom AND M.fonction=Externe.fonction;

 CREATE VIEW vDoctorant AS
SELECT * FROM Interne
WHERE statut->>'type'='Doctorant';

CREATE VIEW vIngenieur AS
SELECT * FROM Interne
WHERE statut->>'type'='Ingenieur';

CREATE VIEW vEnseignantChercheur AS
SELECT * FROM Interne
WHERE statut->>'type'='EnseignantChercheur';

                            /*----------------------------*/
                            /* Vues liées aux contraintes */
                            /*----------------------------*/
                            
CREATE VIEW vValidateur AS
SELECT nomValidateur,fonctionValidateur,projet
FROM Depense;

CREATE VIEW vDemandeur AS
SELECT nomDemandeur,fonctionDemandeur,projet
FROM Depense;

CREATE VIEW vValidateurDansMembreProjet AS
SELECT * 
FROM vValidateur V
LEFT JOIN MembreContribueAProjet M
ON (V.nomValidateur=M.nomMembre AND V.fonctionValidateur=M.fonctionMembre)
WHERE M.nomMembre IS NULL AND M.fonctionMembre IS NULL;
/*Affiche les validateurs qui ne font pas partie du projet dont ils ont validé la dépense */

CREATE VIEW vDemandeurDansMembreProjet AS
SELECT * 
FROM vDemandeur D
LEFT JOIN MembreContribueAProjet M
ON (D.nomDemandeur=M.nomMembre AND D.fonctionDemandeur=M.fonctionMembre)
WHERE M.nomMembre IS NULL AND M.fonctionMembre IS NULL;
/*Affiche les demandeurs qui ne font pas partie du projet dont ils ont validé la dépense */

CREATE VIEW vDepensesProjet AS
SELECT P.nom, B.montant AS BudgetTotal, SUM(D.montant)
FROM vProjet P, Budget B, Depense D
WHERE P.budget=B.id AND P.nom=D.projet
GROUP BY P.nom,P.budget,B.montant;
/* Permet d'obtenir la somme des dépenses réalisées pour un projet */

CREATE VIEW vDateProposition AS
SELECT P.date,A.dateLancement,dateLancement+A.periode AS DateFin
FROM Proposition P, AppelProjet A
WHERE P.themeAppel=A.theme AND P.descriptionAppel=A.description AND P.date>dateLancement+A.periode;
/* Affiche les propositions faites en dehors du créneau défini par l'appel de projet correspondant*/

CREATE VIEW vOrganismeEstFinance AS
SELECT O.nom
FROM OrganismeDeProjet O
LEFT JOIN FinanceOrganisme F
ON (F.nomOrganisme=O.nom)
WHERE F.nomFinanceur IS NULL;
/*Affiche les organismes qui ne sont pas financés par un financeur*/

CREATE VIEW vLabelDelivreParEntite AS
SELECT L.titre
FROM Label L
LEFT JOIN EntiteDelivreLabel E
ON (L.titre=E.titreLabel)
WHERE E.nomEntite IS NULL;
/* Affiche les labels qui n'ont pas été délivré par une entité juridique*/

CREATE VIEW vFinanceurSansEmploye AS
SELECT F.nomEntite
FROM Financeur F
LEFT JOIN EmployeContact E
ON (F.nomEntite=E.financeur)
WHERE E.financeur IS NULL;
/* Affiche les financeurs qui n'ont pas d'employé contact*/

CREATE VIEW vPropositionParInterne AS
SELECT P.titre
FROM Proposition P
LEFT JOIN InternePropose I
ON (I.titreProposition=P.titre)
WHERE I.titreProposition IS NULL;
/*Affiche les propositions qui ne sont pas proposées par des internes */

                            /*-----------------------*/
                            /* Insertion des données */
                            /*-----------------------*/

INSERT INTO Budget VALUES (1,15000,
'[{"montant":10000,"objet":"Salaire","type":"Fonctionnement"},
{"montant":5000,"objet":"Ordinateur","type":"Materiel"}]' ),
(2,75000,'[{"montant":10000,"objet":"Salaire","type":"Fonctionnement"},
{"montant":50000,"objet":"Van","type":"Materiel"},
{"montant":200,"objet":"Blouse","type":"Materiel"}]'),
(14,100000,'{"montant":10000,"objet":"Salaire","type":"Fonctionnement"}'),
(3,10000,'{"montant":10000,"objet":"Salaire","type":"Fonctionnement"}');

INSERT INTO EntiteJuridique VALUES ('ONU'),
('OMS'),
('France'),
('UTC'),
('NASA');

INSERT INTO Financeur VALUES ('ONU','1945-10-24','2020-05-28'),
('OMS','1948-4-7','2020-05-28'),
('UTC','1972-10-2','2020-05-28'),
('France','1792-09-22','2020-05-28'),
('NASA','1958-07-29','2020-05-28');

INSERT INTO EmployeContact VALUES ('PrésidentRepublique','emmanuel.macron@republique.fr','0650407080','France'),
('ChefFusee','responsable.fusee@nasa.com','0650407080','NASA'),
('ChefStationSpatial','responsable.iss@nasa.com','0650407080','NASA'),
('PrésidentUTC','philippe.courtier@utc.fr','0650407080','UTC'),
('Premier Ministre','edouard.philippe@republique.fr','0650407080','France');

INSERT INTO OrganismeDeProjet VALUES ('Laboratoire CNRS1','2000-01-01','100',TRUE),
('Laboratoire CNRS2','2000-01-01','100',TRUE),
('SpaceX','2000-01-01','100',TRUE),
('Star Wars','2000-01-01','100','0');
INSERT INTO OrganismeDeProjet(nom,creation,existe) VALUES ('Laboratoire CNRS3','2000-01-01',TRUE);
INSERT INTO OrganismeDeProjet VALUES ('Laboratoire CNRS4','2000-01-01','100',TRUE);

INSERT INTO AppelProjet VALUES ('Etablir une colonie sur Mars','Definir le plan de vol parfait jusqu a Mars','2020-05-28',100,'SpaceX'),
('Etablir une colonie sur Mars','Recruter les meilleurs colons pour faire partie du premier vol','2020-05-28',100,'SpaceX'),
('Etablir une colonie sur Mars','Construire un vaisseau capable de réaliser le voyage','2020-05-28',100,'SpaceX'),
('Calculer l incertitude','A l aide de modèles probabilistes, réussir à approximer l incertitude','2020-05-28',100,'Laboratoire CNRS3'),
('Renverser l empire','Former la rébellion','2020-05-28',100,'Star Wars');

INSERT INTO Proposition VALUES ('Utiliser les méthodes de l IA','2020-05-29','2020-06-30',FALSE,3,'Calculer l incertitude','A l aide de modèles probabilistes, réussir à approximer l incertitude'),
('Appeler Han Solo','2020-05-29','2020-06-30',TRUE,1,'Renverser l empire','Former la rébellion');
INSERT INTO Proposition VALUES ('Maitriser la force','2021-05-29','2021-06-30',TRUE,2,'Renverser l empire','Former la rébellion');

INSERT INTO Projet VALUES ('Appeler Han Solo','2020-06-30','2021-07-30','Appeler Han Solo'),
('Utiliser les méthodes de l IA','2020-06-30','2021-07-30','Utiliser les méthodes de l IA');

INSERT INTO Membre VALUES ('Einstein','Scientifique','albert.einstein@recherche.com'),
('Vast','Etudiant','mathias.vast@etu.fr'),
('Denoeux','Chercheur','thierry.denoeux@recherche.com'),
('Julia','Chercheur','luc.julia@recherche.com'),
('Hawking','Scientifique','stephen.hawking@recherche.com'),
('Carrez','Etudiant','emilien.carrez@etu.fr');

INSERT INTO Interne VALUES
('Vast','Etudiant','{"type":"Ingenieur", "specialite":"Informatique"}'),
('Carrez','Etudiant','{"type":"Ingenieur", "specialite":"Informatique"}'),
('Denoeux','Chercheur','{"type":"EnseignantChercheur", "quotite":45, "etablissement":"Université de Technologie de Compiègne"}');

INSERT INTO Externe VALUES ('Einstein','Scientifique'),
('Julia','Chercheur'),
('Hawking','Scientifique');

INSERT INTO Depense VALUES ('2020-08-15',500,'Materiel','Appeler Han Solo','Vast','Etudiant','Denoeux','Chercheur'),
('2020-08-20',1000,'Fonctionnement','Appeler Han Solo','Vast','Etudiant','Einstein','Scientifique');
INSERT INTO Depense VALUES ('2020-08-15',1000,'Materiel','Appeler Han Solo','Carrez','Etudiant','Julia','Chercheur');
INSERT INTO Depense VALUES ('2020-09-15',500,'Materiel','Appeler Han Solo','Vast','Etudiant','Denoeux','Chercheur');

INSERT INTO MembreContribueAProjet VALUES ('Vast','Etudiant','Appeler Han Solo'),
('Denoeux','Chercheur','Appeler Han Solo'),
('Einstein','Scientifique','Appeler Han Solo');
INSERT INTO MembreContribueAProjet VALUES ('Vast','Etudiant','Utiliser les méthodes de l IA');


INSERT INTO InternePropose VALUES ('Carrez','Etudiant','Appeler Han Solo'),
('Denoeux','Chercheur','Utiliser les méthodes de l IA');

INSERT INTO Label VALUES ('IA'),
('Probabilité'),
('Espace'),
('Vaisseau'),
('Sabre Laser');
INSERT INTO Label VALUES ('Sécurité');

INSERT INTO LabelProposition VALUES ('Utiliser les méthodes de l IA','IA'),
('Utiliser les méthodes de l IA','Probabilité'),
('Appeler Han Solo','Espace'),
('Appeler Han Solo','Vaisseau'),
('Appeler Han Solo','Sabre Laser');

INSERT INTO EntiteDelivreLabel VALUES ('UTC','IA'),
('UTC','Probabilité'),
('NASA','Espace'),
('NASA','Vaisseau'),
('NASA','Sabre Laser');

INSERT INTO FinanceOrganisme VALUES ('UTC','Laboratoire CNRS1'),
('UTC','Laboratoire CNRS2'),
('UTC','Laboratoire CNRS3'),
('France','Laboratoire CNRS1'),
('France','Laboratoire CNRS2'),
('France','Laboratoire CNRS3'),
('NASA','SpaceX'),
('NASA','Star Wars'),
('ONU','Star Wars');

                            /*-----------------------*/
                            /* Requêtes statistiques */
                            /*-----------------------*/

/*Contient les projets dont le budget n'est pas épuisé*/
CREATE VIEW vBudgetNonNul AS
SELECT P.nom, P.BudgetTotal, P.sum AS DepensesTotales
FROM vDepensesProjet P
WHERE P.sum<P.BudgetTotal;

/*Contient les projets qui ne sont pas finis*/
CREATE VIEW vProjetEnCours AS
SELECT P.nom,P.dateFin 
FROM Projet P
WHERE P.dateFin>DATE(NOW());

/*Contient les appels qui n'ont pas encore donné lieu à des projets et qui sont toujours en cours*/
CREATE VIEW vAppelEnCoursSansReponse AS
SELECT A.theme,A.description
FROM AppelProjet A
LEFT JOIN Proposition P 
ON (A.theme=P.themeAppel)
WHERE P.themeAppel IS NULL AND A.dateLancement+A.periode>DATE(NOW());

/*Contient le nombre de proposition acceptée pour chaque organisme de projet*/
CREATE VIEW vPropSelonOrganisme AS
SELECT O.nom, COUNT(P.titre)
FROM OrganismeDeProjet O, Proposition P, AppelProjet A
WHERE O.nom=A.organisme AND A.theme=P.themeAppel
GROUP BY O.nom;

/*Renvoie le nombre de projet en cours par membret*/
CREATE VIEW vProjetParMembre AS
SELECT M.nomMembre, COUNT(PE.nom) AS nb
FROM MembreContribueAProjet M, vProjetEnCours PE
WHERE M.nomProjet=PE.nom
GROUP BY M.nomMembre;

/*Renvoie le membre avec le plus de projet en cours*/
CREATE VIEW vMembrePlusActif AS
SELECT nomMembre, nb
FROM vProjetParMembre
WHERE nb=(SELECT MAX(nb) FROM vProjetParMembre);

/*Renvoie les dépenses faites par mois*/
CREATE VIEW vDepenseParMois AS
SELECT to_char(date, 'TMMonth') AS "mois", SUM(montant)
FROM Depense
GROUP BY mois;

/*Renvoie le mois de l'année où il y a eu le plus de dépense*/
CREATE VIEW vMoisAvecPlusDepense AS
SELECT mois, sum
FROM vDepenseParMois
WHERE sum=(SELECT MAX(sum) FROM vDepenseParMois);

                            /*--------------------*/
                            /* Gestion des droits */
                            /*--------------------*/

CREATE USER informaticien;
CREATE USER direction;
CREATE USER entite;
CREATE USER membreInterne;
CREATE USER membreExterne;

GRANT ALL PRIVILEGES ON Budget,EntiteJuridique,Financeur,EmployeContact,OrganismeDeProjet,AppelProjet,Proposition,Projet,Label,Membre,Interne,Externe,Depense,MembreContribueAProjet,
InternePropose, LabelProposition,EntiteDelivreLabel,FinanceOrganisme TO informaticien;
GRANT SELECT,INSERT,UPDATE ON  Budget,EntiteJuridique,Financeur,EmployeContact,OrganismeDeProjet,AppelProjet,Proposition,Projet,Label,Membre,Interne,Externe,Depense,MembreContribueAProjet,
InternePropose, LabelProposition,EntiteDelivreLabel,FinanceOrganisme  TO direction;
GRANT SELECT ON Projet,Proposition,Label,OrganismeDeProjet,AppelProjet TO entite;
GRANT INSERT ON OrganismeDeProjet,AppelProjet TO entite;
GRANT SELECT ON Projet,Proposition,Label,OrganismeDeProjet,AppelProjet,Budget,Depense,Membre,Interne,Externe TO membreInterne;
GRANT SELECT ON Projet,Proposition,Label,OrganismeDeProjet,AppelProjet,Budget,Depense,Membre,Interne,Externe TO membreExterne;
GRANT INSERT ON Proposition TO membreInterne;
GRANT INSERT ON Depense TO membreInterne;
GRANT INSERT ON Depense TO membreExterne;




